import mongoose from "mongoose";
const Books = mongoose.model("Books");
const Loans = mongoose.model("Loans");

exports.get_all_books = (req, res) => {
	const query_date = req.query.LoanDate;

	if (!query_date || !query_date.length) {
		Books.find({}, (err, books) => {
			if (err) res.send(err);
			res.json(books);
		});
	} else {
		const date_array = query_date.split("-");
		const date_format = new Date(
			date_array[0],
			date_array[1] - 1,
			date_array[2]
		);

		Loans.find(
			{
				$or: [
					{ date: { $lte: date_format }, returned: { $gte: date_format } },
					{ date: { $lte: date_format }, returned: null }
				]
			},
			(err, loans) => {
				const book_ids = loans.map(loan => {
					return loan.book_id;
				});
				console.log(book_ids);
				if (err) {
					res.status(404).json({ error: "No loans found" });
				} else {
					Books.find({ _id: { $in: book_ids } }, (err, books) => {
						if (err) {
							res.status(404).json({ error: "No books found" });
						} else {
							if (!books || !books.length) {
								res.status(404).json({ error: "No books found" });
							} else {
								res.json(books);
							}
						}
					});
				}
			}
		);
	}
};

exports.get_book_by_id = (req, res) => {
	const bookId = req.params.bookId;

	Books.findById(bookId, (err, book) => {
		if (err) {
			res.status(404).json({ error: "could not find book id" });
		} else {
			res.json(book);
		}
	});
};

exports.get_book_by_name = (req, res) => {
	const bookName = req.params.bookName;

	Books.findOne({ title: bookName }, (err, book) => {
		const { _id } = book;
		res.json({ id: _id });
	});
};

exports.delete_book_by_id = (req, res) => {
	const bookId = req.params.bookId;
	if (
		Books.findById(bookId, (err, book) => {
			if (err) {
				res.status(404).json({ error: "Book id was not found." });
			} else {
				Books.remove({ _id: bookId }, (err, book) => {
					if (err) {
						res.status(500).json({ error: "Could not remove book" });
					} else {
						res.json({ success: "Book was succesfully removed" });
					}
				});
			}
		})
	);
};

exports.edit_book_by_id = (req, res) => {
	const bookId = req.params.bookId;
	if (!bookId) {
		res.status(400).json({ error: "Book id was not found." });
	}

	const { title, author_first, author_last, publish_date, isbn } = req.body;

	if (!title || !title.length) {
		res.status(400).json({ error: "Title required" });
	} else if (!author_first || !author_first.length) {
		res.status(400).json({ error: "First name required" });
	} else if (!publish_date || !publish_date.length) {
		res.status(400).json({ error: "Publish date required" });
	} else if (!isbn || !isbn.length) {
		res.status(400).json({ error: "ISBN required" });
	} else {
		const date_format = publish_date.split("-");
		const actual_date = new Date(
			date_format[0],
			date_format[1] - 1,
			date_format[2]
		);

		Books.findOneAndUpdate(
			{ _id: bookId },
			{ title, author_first, author_last, publish_date: actual_date, isbn },
			{ new: true },
			(err, book) => {
				if (err) {
					res.status(500).json({ error: "Could not update book" });
				} else {
					res.json(book);
				}
			}
		);
	}
};

exports.add_a_book = (req, res) => {
	const { title, author_first, author_last, publish_date, isbn } = req.body;

	if (!title || !title.length) {
		res.status(400).json({ error: "Title required" });
	} else if (!author_first || !author_first.length) {
		res.status(400).json({ error: "First name required" });
	} else if (!publish_date || !publish_date.length) {
		res.status(400).json({ error: "Publish date required" });
	} else if (!isbn || !isbn.length) {
		res.status(400).json({ error: "ISBN required" });
	} else {
		const date_format = publish_date.split("-");
		const actual_date = new Date(
			date_format[0],
			date_format[1] - 1,
			date_format[2]
		);

		new Books({
			title,
			author_first,
			author_last,
			publish_date: actual_date,
			isbn
		}).save((err, new_book) => {
			if (err) {
				res.status(500).json({ error: "Failed to save to database" });
			} else {
				const {
					title,
					author_first,
					author_last,
					publish_date,
					isbn,
					_id
				} = new_book;
				res.json({ title, author_first, publish_date, id: _id });
			}
		});
	}
};
