import mongoose from "mongoose";
import { reviewSchema } from "./../Models/reviewModel";
import { userSchema } from "./../Models/userModel";
import { bookSchema } from "./../Models/bookModel";

const Reviews = mongoose.model("Reviews", reviewSchema);
const Users = mongoose.model("Users", userSchema);
const Books = mongoose.model("Books", bookSchema);

exports.get_user_reviews = (req, res) => {
  Reviews.find({ userId: req.params.user_id }, (err, reviews) => {
    if (err) res.send(err);
    res.json(reviews);
  });
};

exports.get_book_review = (req, res) => {
  Reviews.find(
    { userId: req.params.user_id, bookId: req.params.book_id },
    (err, reviews) => {
      if (err) {
        res.status(404).json({ error: "User of book not found" });
      } else if (reviews.length == 0) {
        res.status(404).json({ error: "User of book not found" });
      } else {
        res.json(reviews);
      }
    }
  );
};

exports.add_a_review = (req, res) => {
  const { rating } = req.body;
  if (!rating) {
    res.status(400).json({ error: "Review must have a rating" });
  } else {
    Users.find({ _id: req.params.user_id }, (err, user) => {
      if (err) {
        res.status(404).json({ error: "User not found" });
      } else {
        Books.find({ _id: req.params.book_id }, (err, book) => {
          if (err) {
            res.status(404).json({ error: "Book not found" });
          } else {
            new Reviews({
              bookId: req.params.book_id,
              userId: req.params.user_id,
              rating
            }).save((err, new_review) => {
              if (err) {
                res.status(500).json({ error: "Failed to save to database" });
              } else {
                const { bookId, userId, rating, _id } = new_review;
                res.json({ bookId, userId, rating, id: _id });
              }
            });
          }
        });
      }
    });
  }
};

exports.delete_review = (req, res) => {
  Users.find({ _id: req.params.user_id }).exec((err, user) => {
    if (err) {
      res.status(404).json({ error: "User not found" });
    } else {
      Books.find({ _id: req.params.book_id }).exec((err, book) => {
        if (err) {
          res.status(404).json({ error: "Book not found" });
        } else {
          Reviews.findOne({
            bookId: req.params.book_id
          }).exec((err, review) => {
            if (err) {
              res
                .status(404)
                .json({ error: "Review with that book ID not found" });
            } else {
              Reviews.findOne({
                userId: req.params.user_id
              }).exec((err, review) => {
                if (err) {
                  res.status(404).json({ error: "Review not found" });
                } else {
                  Reviews.remove({
                    bookId: req.params.book_id,
                    userId: req.params.user_id
                  }).exec((err, review) => {
                    if (err) {
                      res
                        .status(500)
                        .json({ error: "Could not delete review" });
                    } else {
                      res.json({
                        success: "Review was succesfully deleted",
                        review
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};

exports.edit_review = (req, res) => {
  const book_id = req.params.book_id;
  const user_id = req.params.user_id;
  if (!book_id) {
    res.status(400).json({ error: "Book id was not found." });
  } else if (!user_id) {
    res.status(400).json({ error: "User id was not found." });
  }

  const { rating } = req.body;

  if (!rating) {
    res.status(400).json({ error: "Rating required" });
  } else {
    Reviews.findOneAndUpdate(
      { userId: user_id, bookId: book_id },
      { rating },
      { new: true },
      (err, review) => {
        if (err) {
          res.status(500).json({ error: "Could not update review" });
        } else {
          res.json(review);
        }
      }
    );
  }
};

exports.get_all_book_reviews = (req, res) => {
  Reviews.find({}, (err, reviews) => {
    if (err) res.send(err);
    res.json(reviews);
  });
};

exports.get_all_reviews_by_id = (req, res) => {
  const book_id = req.params.book_id;

  if (!book_id) {
    res.status(400).json({ error: "No book id given" });
  } else {
    Reviews.find({ bookId: book_id }, (err, review) => {
      if (err) {
        res.status(404).json({ error: "could not find book" });
      } else {
        res.json(review);
      }
    });
  }
};
