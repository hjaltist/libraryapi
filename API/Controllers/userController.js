import mongoose from "mongoose";
const Users = mongoose.model("Users");
const Books = mongoose.model("Books");
const Loans = mongoose.model("Loans");

exports.get_all_users = (req, res) => {
	const query_date = req.query.LoanDate;
	const loan_duration = req.query.LoanDuration;

	if (!query_date || !query_date.length) {
		if (!loan_duration || !loan_duration.length) {
			Users.find({}, (err, users) => {
				if (err) res.send(err);
				res.json(users);
			});
		} else {
			const today = new Date();
			const days_ago = new Date(
				today.getFullYear(),
				today.getMonth(),
				today.getDay() - parseInt(loan_duration)
			);

			Loans.find({ date: { $lte: days_ago }, returned: null }, (err, loans) => {
				const user_ids = loans.map(loan => {
					return loan.user_id;
				});
				if (err) {
					res.status(404).json({ error: "No loans found" });
				} else {
					Users.find({ _id: { $in: user_ids } }, (err, users) => {
						if (err) {
							res.status(404).json({ error: "No users found" });
						} else {
							res.json(users);
						}
					});
				}
			});
		}
	} else {
		const date_array = query_date.split("-");
		const date_format = new Date(
			date_array[0],
			date_array[1] - 1,
			date_array[2]
		);

		Loans.find(
			{
				$or: [
					{ date: { $lte: date_format }, returned: { $gte: date_format } },
					{ date: { $lte: date_format }, returned: null }
				]
			},
			(err, loans) => {
				const user_ids = loans.map(loan => {
					return loan.user_id;
				});
				console.log(user_ids);
				if (err) {
					res.status(404).json({ error: "No loans found" });
				} else {
					Users.find({ _id: { $in: user_ids } }, (err, users) => {
						if (err) {
							res.status(404).json({ error: "No users found" });
						} else {
							res.json(users);
						}
					});
				}
			}
		);
	}
};

exports.get_user_by_name = (req, res) => {
	const userName = req.params.userName;

	Users.findOne({ first_name: userName }, (err, user) => {
		const { _id } = user;

		res.json({ id: _id });
	});
};

exports.add_a_user = (req, res) => {
	const { first_name, last_name, email, address } = req.body;
	if (!first_name || !first_name.length) {
		res.status(400).json({ error: "First name required" });
	} else if (!last_name || !last_name.length) {
		res.status(400).json({ error: "Last name required" });
	} else if (!email || !email.length) {
		res.status(400).json({ error: "Email required" });
	} else if (!address || !address.length) {
		res.status(400).json({ error: "Address required" });
	} else {
		new Users({
			first_name,
			last_name,
			email,
			address
		}).save((err, new_book) => {
			if (err) {
				res.status(500).json({ error: "Failed to save to database" });
			} else {
				const { first_name, email, _id } = new_book;
				res.json({ first_name, email, id: _id });
			}
		});
	}
};

exports.get_user_by_id = (req, res) => {
	const userId = req.params.userId;

	if (!userId) {
		res.status(400).json({ error: "No user id passed as parameter" });
	} else {
		Users.findById(userId, (err, user) => {
			if (err) {
				res.status(500).json({ error: "Could not find user" });
			} else {
				res.json(user);
			}
		});
	}
};

exports.delete_user_by_id = (req, res) => {
	const userId = req.params.userId;

	Users.remove({ _id: userId }, (err, user) => {
		if (err) {
			res.status(500).json({ error: "Could not remove user" });
		} else {
			res.json({ success: "User deleted", id: userId });
		}
	});
};

exports.edit_user_by_id = (req, res) => {
	const userId = req.params.userId;

	const { first_name, last_name, email, address } = req.body;

	if (!first_name || !first_name.length) {
		res.status(400).json({ error: "First name required" });
	} else if (!last_name || !last_name.length) {
		res.status(400).json({ error: "Last name required" });
	} else if (!email || !email.length) {
		res.status(400).json({ error: "Email required" });
	} else if (!address || !address.length) {
		res.status(400).json({ error: "Address required" });
	}

	Users.findOneAndUpdate(
		{ _id: userId },
		{ first_name, last_name, email, address },
		{ new: true },
		(err, user) => {
			if (err) {
				res.status(500).json({ error: "Could not update user" });
			} else {
				res.json(user);
			}
		}
	);
};

exports.register_loan = (req, res) => {
	const userId = req.params.userId;
	const bookId = req.params.bookId;

	Users.find({ _id: userId }, (err, user) => {
		if (err) {
			res.status(404).json({ error: "User not found" });
		} else {
			Books.find({ _id: bookId }, (err, book) => {
				if (err) {
					res.status(404).json({ error: "Book not found" });
				} else {
					const date = new Date();
					const is_returned = null;
					new Loans({
						user_id: userId,
						book_id: bookId,
						returned: is_returned,
						date
					}).save((err, new_loan) => {
						if (err) {
							res.status(500).json({ error: "Could not save to database" });
						} else {
							const { user_id, book_id, _id } = new_loan;
							res.json({ userId: user_id, bookId: book_id, id: _id });
						}
					});
				}
			});
		}
	});
};

exports.get_books_user_has_on_loan = (req, res) => {
	const userId = req.params.userId;

	Loans.find({ user_id: userId }, (err, loan) => {
		if (err) {
			res.statur(404).json({ error: "User has no loans" });
		} else {
			res.json(loan);
		}
	});
};

exports.delete_loan_by_id = (req, res) => {
	const userId = req.params.userId;
	const bookId = req.params.bookId;

	const { date } = new Date();

	if (!date || !date.length) {
		res.status(400).json({ error: "date required" });
	}

	Loans.findOneAndUpdate(
		{ book_id: bookId, user_id: userId },
		{ date },
		{ new: true },
		(err, loan) => {
			if (err) {
				res.status(500).json({ error: "Could not edit loan" });
			} else {
				res.json({ loan });
			}
		}
	);
};

exports.edit_loan_by_id = (req, res) => {
	const userId = req.params.userId;
	const bookId = req.params.bookId;

	const { date } = req.body;

	if (!date || !date.length) {
		res.status(400).json({ error: "date required" });
	}

	Loans.findOneAndUpdate(
		{ book_id: bookId, user_id: userId },
		{ date },
		{ new: true },
		(err, loan) => {
			if (err) {
				res.status(500).json({ error: "Could not edit loan" });
			} else {
				res.json({ loan });
			}
		}
	);
};
