import mongoose from "mongoose";

const bookSchema = mongoose.Schema({
	title: String,
	author_first: String,
	author_last: String,
	publish_date: Date,
	isbn: String
});

export { bookSchema };
