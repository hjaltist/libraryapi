import mongoose from "mongoose";

const loanSchema = mongoose.Schema({
	book_id: String,
	user_id: String,
	date: Date,
	returned: Date
});

export { loanSchema };
