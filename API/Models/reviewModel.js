import mongoose from "mongoose";

const reviewSchema = mongoose.Schema({
  bookId: String,
  userId: String,
  rating: Number
});

export { reviewSchema };
