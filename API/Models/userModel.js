import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
	first_name: String,
	last_name: String,
	email: String,
	address: String
});

export { userSchema };