module.exports = app => {
  var bookController = require("./API/Controllers/bookController");
  var userController = require("./API/Controllers/userController");
  var reviewController = require("./API/Controllers/reviewController");

  // Review routes
  app.route("/users/:user_id/reviews").get(reviewController.get_user_reviews);

  app
    .route("/users/:userId")
    .get(userController.get_user_by_id)
    .delete(userController.delete_user_by_id)
    .put(userController.edit_user_by_id);

  app
    .route("/users/:user_id/reviews/:book_id")
    .post(reviewController.add_a_review)
    .get(reviewController.get_book_review)
    .delete(reviewController.delete_review)
    .put(reviewController.edit_review);

  app.route("/books/reviews").get(reviewController.get_all_book_reviews);

  app
    .route("/books/:book_id/reviews")
    .get(reviewController.get_all_reviews_by_id);

  // Book Routes
  app
    .route("/books")
    .get(bookController.get_all_books)
    .post(bookController.add_a_book);

  app
    .route("/books/:bookId")
    .get(bookController.get_book_by_id)
    .put(bookController.edit_book_by_id)
    .delete(bookController.delete_book_by_id);

  // User Routes
  app
    .route("/users")
    .get(userController.get_all_users)
    .post(userController.add_a_user);

  app
    .route("/users/:userId/books/:bookId")
    .post(userController.register_loan)
    .delete(userController.delete_loan_by_id)
    .put(userController.edit_loan_by_id);

  app.route("/books/:bookName/name").get(bookController.get_book_by_name);

  app.route("/users/:userName/name").get(userController.get_user_by_name);

  app
    .route("/users/:userId/books")
    .get(userController.get_books_user_has_on_loan);
};
