import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import { bookSchema } from "./API/Models/bookModel";
import { userSchema } from "./API/Models/userModel";
import { loanSchema } from "./API/Models/loanModel";
import routes from "./routes.js";

const { Schema } = mongoose;

mongoose.Promise = global.Promise;
mongoose
	.connect("mongodb://library:library123@ds241065.mlab.com:41065/libraryapi", {
		useMongoClient: true
	})
	.then(db => {
		console.log("MongoDB connected");
		const books = db.model("Books", bookSchema);
		const users = db.model("Users", userSchema);
		const loans = db.model("Loans", loanSchema);

		const app = express();
		app.use(bodyParser.json());

		routes(app);

		app.listen(3000, () => console.log("Listening to port 3000"));

		//var port = proccess.env.PORT || 3
	});
